import React from 'react';
import {createStyles, makeStyles, Theme} from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import {Typography} from "@material-ui/core";

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        formControl: {
            margin: theme.spacing(1),
            minWidth: 120,
        },
        selectEmpty: {
            marginTop: theme.spacing(2),
        },
    }),
);

export default function CourseForm() {
    const classes = useStyles();
    const [level, setLevel] = React.useState(0);
    const [grade, setGrade] = React.useState(0);
    const [units, setUnits] = React.useState(0);


    const handleLevelChange = (event: React.ChangeEvent<{ value: unknown }>) => {
        setLevel(event.target.value as number);
    };

    const handleGradeChange = (event: React.ChangeEvent<{ value: unknown }>) => {
        setGrade(event.target.value as number);
    };

    const handleUnitChange = (event: React.ChangeEvent<{ value: unknown }>) => {
        setUnits(event.target.value as number);
    };

    const handleChange = () => {

    }

    return (
        <div>
            <FormControl className={classes.formControl}>
                <InputLabel id="demo-simple-select-label">Course Year Level</InputLabel>
                <Select
                    labelId="demo-simple-select-label"
                    id="demo-simple-select"
                    value={level}
                    onChange={handleLevelChange}
                >
                    <MenuItem value={1}>1 (Number code starts with 1)</MenuItem>
                    <MenuItem value={2}>2 (Number code starts with 2)</MenuItem>
                    <MenuItem value={3}>3 (Number code starts with 3)</MenuItem>
                    <MenuItem value={4}>4 (Number code starts with 4 or 6)</MenuItem>
                    <MenuItem value={5}>5 (Number code starts with 7)</MenuItem>
                </Select>
                <FormHelperText>Course codes follow the format XXXX0000. The second half represents the number
                    code.</FormHelperText>
            </FormControl>
            <FormControl className={classes.formControl}>
                <InputLabel id="demo-simple-select-label">Final Grade</InputLabel>
                <Select
                    labelId="demo-simple-select-label"
                    id="demo-simple-select"
                    value={grade}
                    onChange={handleGradeChange}
                >
                    <MenuItem value={1}>1</MenuItem>
                    <MenuItem value={2}>2</MenuItem>
                    <MenuItem value={3}>3</MenuItem>
                    <MenuItem value={4}>4</MenuItem>
                    <MenuItem value={5}>5</MenuItem>
                    <MenuItem value={6}>6</MenuItem>
                    <MenuItem value={7}>7</MenuItem>


                </Select>
            </FormControl>
            <FormControl className={classes.formControl}>
                <InputLabel id="demo-simple-select-label">Units</InputLabel>
                <Select
                    labelId="demo-simple-select-label"
                    id="demo-simple-select"
                    value={units}
                    onChange={handleUnitChange}
                >
                    <MenuItem value={1}>1</MenuItem>
                    <MenuItem value={2}>2</MenuItem>
                    <MenuItem value={4}>4</MenuItem>
                    <MenuItem value={6}>6</MenuItem>
                    <MenuItem value={8}>8</MenuItem>
                </Select>
            </FormControl>
        </div>
    );
}


