import * as React from "react";
import {Theme, createStyles, makeStyles} from '@material-ui/core/styles';
import Accordion from '@material-ui/core/Accordion';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import SingleCourse from "./SingleCourse";
import Card from "@material-ui/core/Card";
import AddCircleIcon from '@material-ui/icons/AddCircle';
import {Button, IconButton} from "@material-ui/core";
import {AddCircle} from "@material-ui/icons";
import DeleteIcon from "@material-ui/icons/Delete";


const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            width: '100%',
        },
        heading: {
            fontSize: theme.typography.pxToRem(15),
            fontWeight: theme.typography.fontWeightRegular,
        },
        formControl: {
            margin: theme.spacing(1),
            minWidth: 120,
        },
        selectEmpty: {
            marginTop: theme.spacing(2),
        },
    }),
);

export default function StudyYear(prop: { yearLevel: number }) {
    const classes = useStyles();
    let year = "Year " + prop.yearLevel;

    const handleDeleteCourse = (id : number) => {
        setCourses([...courses.splice(0, id), ...courses.splice(id+1)]);
    }

    const [courses, setCourses] = React.useState([<SingleCourse/>]);

    const handleAddCourse = () => {
        setCourses([...courses, <SingleCourse/>]);
    }


    const coursesMap = courses.map((course, index) => [
        <Card variant="outlined">
            <Typography>
                Course Number {index + 1}
            </Typography>
            <div onClick={() => handleDeleteCourse(index)}>
                <IconButton aria-label="delete">
                    <DeleteIcon/>
                </IconButton>
            </div>
            {course}
        </Card>
    ]);


    return (
        <div className={classes.root}>
            <Accordion>
                <AccordionSummary
                    expandIcon={<ExpandMoreIcon/>}
                    aria-controls="panel1a-content"
                    id="panel1a-header"
                >
                    <Typography className={classes.heading}>{year}</Typography>
                </AccordionSummary>

                <label htmlFor="contained-button-file">
                    <Button color="primary" onClick={handleAddCourse}>
                        Add Course
                        <AddCircleIcon>
                            <AddCircle/>
                        </AddCircleIcon>
                    </Button>
                </label>
                <label htmlFor="icon-button-file">

                </label>

                <AccordionDetails>
                    <Typography>
                        Input your courses here:
                        {coursesMap}
                    </Typography>

                </AccordionDetails>
            </Accordion>
        </div>
    )
}